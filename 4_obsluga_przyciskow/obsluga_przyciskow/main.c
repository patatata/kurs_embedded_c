/*
 * obsluga_przyciskow.c
 *
 * Created: 2017-09-12 22:10:29
 * Author : Dell
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>

//wciskajac przycisk zwieramy pin procesora do masy, czyli powodujemy stan niski
//drganie stykow - mikrokontroler moze widziec drgania na przyciskach o stanie wysokim/niskim, w celu
//unikniecia niejasnosci nalezy odczekac i sprawdzic ponownie stan pinu do ktorego podlaczony jest przycisk 

#define SW (1<<PC5) //liczba 0b00000001 zostaje przesunieta o 5 miejsc w lewo 0b00100000, czyli 1<<PC5 to samo co 0b00100000
#define KEY_DOWN !(PINC & SW) //PINx jest to rejestr sluzacy do odczytywania stanu pinu WEJSCIOWEGO w tym przypadku pinu na porcie C. Jezeli
//przycisk wcisniety stan pinu5 w PINC = 0, to znaczy PINC = 0bxx0xxxxx (gdzie x to 1 lub 0 w zaleznosci jak pozostale piny zostaly ustawione).
// Piszac w ten sposob sprawdzamy tylko jeden interesujacy nas pin, czyli zakladamy tzw. maske na ten jeden bit. Jezeli przycisk wcisniety PINC5=0.
// Czyli #define KEY_DOWN !(0 & 1). Mamy tutaj iloczyn logiczny, z tabeli prawdy wynika, �e 0&1 daje 0. Pozostaje !(0) = 1, poniewa� ! to negacja. 
// Uzywamy negacji po to bysmy mogli w latwy sposob korzystac z if, poniewaz wejscie do if nastepuje gdy w srodku if jest prawda, czyli logiczne 1.  
#define LED (1<<PB0)
#define TOGLE_LED (PORTB ^= LED)

int main(void)
{
	DDRB = 0x01;
	DDRC = 0;
	PORTC |= SW;
	_delay_ms(10);

	while (1)
	{
		if(KEY_DOWN)
		{
			_delay_ms(100);
			if(KEY_DOWN)
			{
				//PORTB = 0x01;
				TOGLE_LED;
				_delay_ms(200);
			}
		}
	}
}

//Zadanie domowe
//Polaczyc dwie diody led. Nacisniecie przycisku na jednej ma spowodowac zmiane stanu, na drugiej migajacej
// co 1s ma spowodowac zwiekszenie czestotliwosci miganie o 100ms kazde nacisniecie. 
