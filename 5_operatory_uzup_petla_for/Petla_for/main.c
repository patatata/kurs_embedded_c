#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Petla for umozliwia powtorzenie pewnych czynnosci. Bardzo czesto wykorzystywana do operacji na tablicach
	int i; //licznik sluzy do zliczania kolejnych iteracji - obiegow petli for
	//for (inicjalizacja_licznika; warunek konczacy; modyfikacja_licznika)
	//1. Pierwsze wejscie do petli. Inicjalizacja licznika wartoscia poczatkowa.
	//2. Sprawdzanie warunku petli, jezeli prawdziwy to wykonywany jest kod w petli
	//3. Po wykonaniu kodu wykonywana jest modyfikacja licznika
	//4. Po modyfikacji sprawdzenie warunku, jezeli poprawny to wejscie do srodka petli
	for (i = 0; i < 10; ++i) //zmiana licznika moze byc zapisana i=i+2, i--, i++ itd.
	{
	    printf("hej ");
		//printf("%d\n", i);
	}

	//Za pomoca petli for mozna stworzyc petle nieskonczona


    return 0;
}


//Zadanie 1.
//Za pomoca petli for znajdz w zbiore liczb od 0 do 100 liczby parzyste i wypisz je

//Zadanie 2.
//Napisz program ktory wyswietli 10 liczb malejaco
