#include <stdio.h>
#include <stdlib.h>

//na drugim spotkaniu poznalismy:
    //operatory porownania
	// >
	// <
	// >=
	// <=
	// != -> operator ktory sprawdza czy lewa strona jest rozna od prawej
	// == -> operator rownosci

    //operatory && (logiczne i, koniunkcja), || (logiczne lub, alternatywa)
	//&& -> AND (wtedy prawda, kiedy wszystkie warunki sa prawdziwe)
	//0 0 -> 0
	//1 0 -> 0
	//0 1 -> 0
	//1 1 -> 1

    //|| -> OR (wtedy prawda, kiedy przynajmniej jeden jest prawdziwy)
	//0 0 -> 0
	//1 0 -> 1
	//0 1 -> 1
	//1 1 -> 1

	//operator przypisania - przypisuje wartosc prawego argumentu lewemu
	int a = 2;

int main()
{
    //operator inkrementacji, dekrementacji
    printf("Sprawdzanie operatora inkrementacji \n");
    int rok = 2017;
    printf("%d ", rok);
    printf("%d ", rok++); //operator postinkrementacji
    printf("%d ", ++rok); //operator preinkrementacji
    printf("%d ", rok--); //operator postdekrementacji
    printf("%d ", --rok); //operator predekrementacji
    printf("Operator negacji \n");
    //operator negacji
    int r = 34;
	//czy liczba nie jest mniejsza od 10
	//sposob 1
	if (r >= 10)
	{
		printf("Sposob1\n");
	}
	//operator negacji logicznej -> !
	if (!(r < 10)) //sprawdzam czy r jest mniejsze od 10 i jezeli tak to zwraca prawde ale negacja logiczna spowoduje odwrocenie na FALSZ
	{
		printf("Sposob2\n");
	}



	//operator modulo = pozwala obliczyc reszte z dzielenia
	int reszta;
	reszta = 10 % 3;

    //skrocony zapis przy przypisywaniu
    rok = rok +1 ;
    rok += rok; //podobnie dla -=, /=, *=, %=

    return 0;
}


// Praca domowa prosze aby znajdowala sie w oddzielnym projekcie z ta sama nazwa co ten projekt
// i dodatkowo _praca_domowa. Przed kazdym programem prosze wkleic tresc danego zadania

//Zadanie 1.
//Napisz program w ktorym uzytkownik podaje liczbe i sprawdza czy jest podzielna przez 2 i 3
//jezeli tak to wypisz Liczba jest podzielna przez 2 i 3. Jezeli nie to wypisz ze nie podzielna i
//jaka jest reszta

//Zadanie 2.
//Napisz program w ktorym prosisz uzytkownika o podanie liczby a nastepnie sprawdzasz 50 kolejnych
//liczb pod katem tego ile jest nieparzystych w tym zakresie. Jezeli liczba jest nieparzysta
//wypisz ja dodatkowo. Po 50-tej liczbie wypisz koniec oraz ilosc znalezionych nieparzystych liczb



