/*
 * wyswietlacz_7_seg.c
 *
 * Created: 2017-09-17 22:44:41
 * Author : Dell
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>

#define SEG_LEDA (1<<PD0)
#define SEG_LEDB (1<<PD1)
#define SEG_LEDC (1<<PD2)
#define SEG_LEDD (1<<PD3)
#define SEG_LEDE (1<<PD4)
#define SEG_LEDF (1<<PB6)
#define SEG_LEDG (1<<PB7)

//Wyswietlacz siedmiosegmentowy to zbior LED. Jednemu segmentowi odpowiada jedna dioda. Wyrozniamy wyswietlacze
//ze wspolna katoda oraz anoda. 
//Wspolna anoda - wspolnym biegunem wyswietlacza jest anoda, co oznacza ze jest ona podlaczona do zasilania, a 
//wyswietlacz sterowany jest katodami czyli masa. 
//Za zasilanie kazdego wyswietlacza odpowiada tranzystor, ktory musi byc wysterowany. 

int main(void)
{
	DDRB = 0x01;
	DDRC = 0;
	DDRD |= SEG_LEDA; //DDRD = DDRD | SEG_LED //ustawienie na wyjscie 
	DDRD |= SEG_LEDB;
	DDRD |= SEG_LEDC;
	DDRD |= SEG_LEDD;
	DDRD |= SEG_LEDE;
	DDRB |= SEG_LEDF;
	DDRB |= SEG_LEDG;

	PORTD &= SEG_LEDA; //DDRD = DDRD & SEG_LED //
	PORTD &= SEG_LEDB;
	PORTD &= SEG_LEDC;
	PORTD &= SEG_LEDD;
	PORTD &= SEG_LEDE;
	PORTB &= SEG_LEDF;
	PORTB &= SEG_LEDG;
    /* Replace with your application code */
    while (1) 
    {
			
    }
}

