/*
 * GccApplication1.c
 *
 * Created: 2017-09-01 15:18:32
 * Author : EPAPADU
 */ 

#define F_CPU 1000000UL

#include <avr/io.h>
#include <util/delay.h>

//dioda jak kazdy element ma okreslony prad ktory moze przez nia poplynac 
//patrzac od gory na diode nalezy znalezc sciecie i podlaczyc nozke przy nim do masy ale nie bezposrednio

//rezystor tez ma pewien maksymalny prad ktory moze przez niego przeplynac, oblicza sie go na podstawie mocy

//dioda - aby przez diode zaczal plynac prad (zaczela swiecic) nalezy dostarczyc jej odpowiednie napiecie progowe i prad
// czyli najczesciej If=20mA, Uf = 2.1
//czerwona dioda napiecie od 2 do 2.7, zielona 2.3-2.5. Np. 2.4V, na rezystorze powinno zatem byc 2.6V. Liczymy prad,/
//czy prad odpowiedni dla wartosci progowej diody. Dioda zwykle powinna miec okolo 20mA

//anode (dluzsza nozke) laczymy z plusem
//katode laczymy z masa 
//rezystor obojetnie 

//Uwe - Ud = UR
//Uwe - Ud1 - Ud2

#define SW (1<<PC5)
#define KEY_DOWN !(PINC & SW) //makro sprawdzajace stan niski
#define LED (1<<PB0)
#define TOGLE_LED (PORTB ^= LED)

int main(void)
{
	DDRB = 0x01;
	DDRC = 0;

	
    /* Replace with your application code */
    while (1) 
    {
		if(!KEY_DOWN)
		{
			_delay_ms(80);
			if(!KEY_DOWN)
			{
				//PORTB = 0x01;
				TOGLE_LED;
				_delay_ms(100);
			}
		}
//		PORTB = 0x01;
//		_delay_ms(1000);
//		PORTB = 0;
//		_delay_ms(1000);		
    }
}
