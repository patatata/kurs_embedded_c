#include <stdio.h>
#include <stdlib.h>

int main()
{
    //kiedy mamy zrobic na przyklad proste menu i obslugiwac z poziomu programu, dobrym sposobem jest uzycie instrukcji switch
	//instrukcja switch zastepuje nam rozbudowany if

	//nalezy zrobic menu -> kalkulator
	printf("KALKULATOR\n");
	printf("1. Dodawanie\n");
	printf("2. Odejmowanie\n");
	printf("3. Mnozenie\n");
	printf("4. Dzielenie\n");
	printf("5. Pierwiastkowanie\n");
	printf("Twoj wybor\n");
	int wybor;
	scanf_s("%d", &wybor);
	getchar();

	//dwie zmienne ktore wykorzystamy do kalkulatora
	float a;
	float b;
	float c;

	//zadania polegajace na tym, ze masz jedna zmienna i badasz jej wartosc i na tej podstawie wykonujesz rozne operacje
	//mozesz realizowac za pomoca instrukcji switch

	switch (wybor)
	{
	case 1: //wykona sie kiedy masz w zmiennej wybor wartosc 1
		printf("Dodawanie\n");
		printf("Podaj pierwsza liczbe:\n");
		scanf_s("%f", &a);
		getchar();
		printf("Podaj druga liczbe:\n");
		scanf_s("%f", &b);
		getchar();
		printf("%.2f + %.2f = %.2f\n", a, b, a + b);
		break; //break powoduje ze jak w switch zostanie napotkany to przerywa dzialanie switch - wychodzi z niego do kolejnych instrukcji
		//w programie
	case 2: //wykona sie kiedy masz w zmiennej wybor wartosc 2
		printf("Odejmowanie\n");
		printf("Podaj pierwsza liczbe:\n");
		scanf_s("%f", &a);
		getchar();
		printf("Podaj druga liczbe:\n");
		scanf_s("%f", &b);
		getchar();
		printf("%.2f - %.2f = %.2f\n", a, b, a - b);
		break;
	case 3: //wykona sie kiedy masz w zmiennej wybor wartosc 3
		printf("Mnozenie\n");
		printf("Podaj pierwsza liczbe:\n");
		scanf_s("%f", &a);
		getchar();
		printf("Podaj druga liczbe:\n");
		scanf_s("%f", &b);
		getchar();
		printf("%.2f * %.2f = %.2f\n", a, b, a * b);
		break;
	case 4: //wykona sie kiedy masz w zmiennej wybor wartosc 4
		printf("Dzielenie\n");
		printf("Podaj pierwsza liczbe:\n");
		scanf_s("%f", &a);
		getchar();
		printf("Podaj druga liczbe:\n");
		scanf_s("%f", &b);
		getchar();

		if (b != 0)
		{
			printf("%.2f / %.2f = %.2f\n", a, b, a / b);
		}
		else
		{
			printf("Nie dziel przez 0\n");
		}
		break;
		//aby zrealizowac pierwiastkowanie musimy zastosowac specjalna biblioteke oi nazwie math.h
		//posiada ona zestaw najczesciej wykorzystywanych funkcji matematycznych
	case 5: //wykona sie kiedy masz w zmiennej wybor wartosc 5
		printf("Pierwiastkowanie\n");
		printf("Podaj liczbe, ktora chcesz spierwiastkowac:\n");
		scanf_s("%f", &c);
		getchar();
		printf("Pierwiastek z liczby %.2f wynosi %.2f\n", c, sqrt(c));

		break;
	default: //wykona sie kiedy zmienna wybor ma inna wartosc niz rozpatrzone powyzej
		printf("Nie ma takiej opcji\n");
	}
/*
zad 1
napisz program, w ktorym robisz menu, za pomoca switch wybierasz jedna z opcji
1. Wiek
2. Waga
3. Srednia rytmetyczna
4. Iloczyn
5. Wieksza z dwoch
6. Czy rowne

Ad 1.
Prosisz uzytkownika o podanie wieku i jezeli poda wiek powyzej 20 to piszesz mu ze jest stary, a jezeli ma mniej lub 20 to jest pisz
komunika ze jest mlody

Ad 2.
Prossisz o podanie wagi i jezeli waga wieksza od 80 to piszesz "GRUBAS" a w przeciwnym razie piszesz "CHUDY"

Ad 3.
Prosisz uzytkownika o podanie trzech liczb i wyswietlasz ich srednia arytmetyczna

Ad 4.
Prosisz o podanie trzech liczb i wyswietlasz ich iloczyn

Ad 5.
Prosisz o podanie dwoch liczb i wyswietlasz wieksza z nich

Ad 6.
Prosisz o podanie dwoch liczb i jezeli sa rowne wyswietlasz "ROWNE" a jak nie to "NIE SA ROWNE"
*/
    return 0;
}
