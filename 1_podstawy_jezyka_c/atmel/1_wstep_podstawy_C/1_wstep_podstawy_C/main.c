#include <stdio.h>
#include <stdlib.h>

//8-bitowy mikrokontroler - tzn. ze jego jednostka arytmetyczna ALU wykonuje
// operacje glownie na 8-bitowych liczbach. Rejestry takiego mikrokontrolera sa max 8-bitowe
// rodzina atmeg ma mikrokontroler z wyprowadzeniami od 28 do 100 pinow
// mamy dwa rodzaje pinow: analogowe (np. do pamiaru napiecia_ i cyfrowe (moga miec tylko wartosc 0 albo 1)

// na pisach procesora mozemy ustawic stan niski badz wysoki
// pinowi mozna przypisac kierunek:
// wyjscie - sygnaly wychodzace z uc
// wejscie - sygnaly przesylane do uc

//pamieci
//FLASH - tam wgrywamy kod programu, stale, lancuchy znakowe, jest to pamiec nieulotna, dane sa
// sa przechowywane rowniez po zaniku zasilania
//RAM - SRAM - tam sa przechowywane zmienne, dane przepadaja po zaniku zasilania

unsigned char sek; //zmienna globalna, to jest zmienna widziana przez wszystki funkcje, przechowywana jest w ramie,
// kompilator inicjalizuje zmienne globalne wartoscia 0., Zmienna jest zadelkarowana poza funkja.

int main()
{
    int counter; //zmienna lokalna nie sa inicjalizowane jako 0 prez co jeli brak wartosci dla tej zmiennej
    // to w tej smiennej beda smieci, czyli jakies przykladowe wartosci z pamieci...
    printf("%d" ,counter); //%d dla int, %c dla char, %f dla float
    unsigned int a = 2; //sluzy do przechowywania liczb dodatnich // znalezc jaki przedzial liczbowy jest dla tego typu
    int b = -1000; //przechowuje liczby calkowite ze znakiem // wypisac przedzial,    //
    //4 bajty	-231÷ 231 - 1, czyli przedział [-2147483648, 2147483647]
    long int c = 100000; // do przechowywania liczb bardzo duzych
    //4 bajty	-231÷ 231 - 1, czyli przedział [-2147483648, 2147483647]
    float d = 2.56; //przechowuje liczby zmiennnoprzecinkowe //przedzial napisac
    //4 bajty	pojedyncza precyzja - dokładność 6 - 7 cyfr po przecinku
    double e = 2.56774; //przechowuje liczby zmiennoprzecinkowe // sprawdzic max dokladnosc po przecinku
    //8 bajty	podwójna precyzja - dokładność 15 - 16 cyfr po przecinku
    printf("Hello world!\n");
    return 0;
}



//co bedzie potrzebne:
//atmega328
//usbasp avr - programator
//EAGLE

//Zadania
// poszukaj jak sie nazywa rejestr zajmuacy sie ustawianiem kierunku pinu
// kierunek pinu okresla rejest DDRx gdzie x oznacza konkretny port A,B,C,D. ok
//sprawdz jak defaultowo sa ustawione kierunki pinow na przyklad na porcie B
//defaultowo port B ustawiony jest na 0 czyli Wejscie ok
//sprawdzic ile pamieci flash ma atmega328 (na poczatku samym)
//32 KBytes
//jaka jest roznica pomiedzy pamiecia SRAM a RAM
// pamiec SRAM ( Static Random Access Memory) sluzy jako pamiec buforujaca miedzy pamiecia a procesorem.
// SRAM posiada szybszy odczyt 7  razy niz RAM, nie potrzebuje odswiezenia
// DRAM (Dynamic Access Memory) w odroznieiu od SRAM nie potrzebuje stalego zasilania, na tomiast
//okresowego odwiezania wartosci. Dzieki temu potrzebuje mniej energii. DRAM jest prostszy w
//budowie(mniejsza ilosc elementow) niz SDRAM. Mozliwe przez to wieksze upakowanie DRAM  w ukladach scalonych co niesie
//za soba nizsze koszty i pozwala na budowe pamiecie o wiekszej pojemnosci. ok
//

// napisac przy kazdym typie ile ma bajtow
