#include <stdio.h>
#include <stdlib.h>

// na poczatek 8-bitowa atmega od 28 do 100 pinow
// 8-bitowy mikrokontroler tzn. ze jego jednostka arytmetyczna operuje na 8-bitowych liczbach
// zwykle ma 8-bitowe porty, rejestry
// atmega328p ma 28 pinow (DIP), 32kB pamieci flash
// Piny mozna ustawiac jako wyjscie (sygnaly wychodzace z uc), wejscie (sygnaly przesylane do uc, np.guziki)
// pelnic funkcje: wejscie analogowe pomiar napiecie (z uzyciem przetwornika) zast. woltomierz, amperomierz, kontrola poziomu nap. w akumulatorze
// Z kolei na pinach mozna ustawiac stany (wysoki, niski, posredni (pomiedzy gnd a vcc))
// do wgrywania kodu programator np. usbasp
// kazdy program sklada sie z funkcji main
// komentujemy poprzez // /**/ oraz instrukcje preprocesora np. #if 0 #endif


int main()
{
    printf("Hello world!\n");
    return 0;
}
