/*
 * GccApplication1.c
 *
 * Created: 2017-09-01 15:18:32
 * Author : EPAPADU
 */ 

#define F_CPU 1000000UL

#include <avr/io.h>
#include <util/delay.h>



int main(void)
{
	DDRB = 0x01;
	
    /* Replace with your application code */
    while (1) 
    {
		PORTB = 0x01;
		_delay_ms(1000);
		PORTB = 0;
		_delay_ms(1000);		
    }
}
