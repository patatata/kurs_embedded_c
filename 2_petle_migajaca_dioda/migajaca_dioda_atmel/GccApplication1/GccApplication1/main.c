/*
 * GccApplication1.c
 *
 * Created: 2017-09-01 15:18:32
 * Author : EPAPADU
 */ 

#include <avr/io.h>
#include <util/delay.h>


#define F_CPU 1000000UL


int main(void)
{
	DDRB = 0xFF;
    /* Replace with your application code */
    while (1) 
    {
		PORTB0 = 1;
		_delay_ms(1000);
		PORTB0 = 0;
		_delay_ms(1000);		
    }
}

