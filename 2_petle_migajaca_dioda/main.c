#include <stdio.h>
#include <stdlib.h>

int main()
{
    //if sluzy do podejmowania decyzji
	/*if (warunek)
	{
		//wykona sie jezeli warunek jest prawdziwy
	}
	else
	{
		//wykona sie jezeli warunek nie jest prawdziwy
	}*/
    //operatory porownania
	// >
	// <
	// >=
	// <=
	// != -> operator ktory sprawdza czy lewa strona jest rozna od prawej
	// == -> operator rownosci
    printf("Podaj liczbe:\n");
	int x;
	scanf_s("%d", &x);
	getchar();
    if (x > 10)
	{
		//jezeli x jest wiekszy od 10 to w tym momencie wykona sie instrukcja/e ponizej
		printf("Podales liczbe wieksza od 10\n");

	}
	else
	{
		printf("Podales liczbe mniejsza lub rowna od 10\n");
	}
		if (10 == x)
	{
		printf("Operator == dziala!\n");
	}

	//operator !=
	//kiedy lewa strona jest ROZNA od prawej to wtedy wynikiem sprawdzania warunku jest PRAWDA
	//operator != jest przeciwienstwem ==
	if (x != 10)
	{
		printf("Operator != dziala! \n");
	}
		//operatory && (logiczne i, koniunkcja), || (logiczne lub, alternatywa)
	//&& -> AND (wtedy prawda, kiedy wszystkie warunki sa prawdziwe)
	//0 0 -> 0
	//1 0 -> 0
	//0 1 -> 0
	//1 1 -> 1

	//|| -> OR (wtedy prawda, kiedy przynajmniej jeden jest prawdziwy)
	//0 0 -> 0
	//1 0 -> 1
	//0 1 -> 1
	//1 1 -> 1

	//zad 1 -> sprawdzasz czy podana liczba jest w przedziale <9; 14)
	if (x >= 9 && x < 14)
	{
		printf("Nalezy do <9,14)\n");
	}

	//zad 2 -> sprawdzasz czy liczba jest parzysta oraz czy jest mniejsza od 10
	if (x % 2 == 0 && x < 10)
	{
		printf("x mniejszy od 10 i parzysty \n");
	}

	//zad 3 -> liczba przy dzieleniu przez 6 daje reszte 3
	if (x % 6 == 3)
	{
		printf("x przy dzieleniu przez 6 daje reszte 3 \n");
	}

	//zad 4 -> podana liczba jest nieparzysta lub (wieksza od 10 i jednoczesnie mniejsza od 20)
	if (x % 2 == 1 || ( x > 10 && x < 20 ))
	{
		printf("xxx \n");
	}


    return 0;
}


//sprawdzic jaki oscylator jest ustawiony w atmega328
