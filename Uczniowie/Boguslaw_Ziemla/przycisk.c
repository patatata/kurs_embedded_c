/*
 * obsluga_przyciskow.c
 *
 * Created: 2017-09-16 11:51:45
 * Author : Bogu
 */ 

//przyciskajac przycisk zwieramy pin do ktorego polaczony jest przycisk do masy (GND)zasianie (VCC)
//drganie stykow - mikrokontroler moze widziec drgania na przyciskach, aby uniknac tego efektu nalezy 
//odczekac pewien czas (czas podany w datasheecie przycisku) i sprawdzic ponownie stan, czy 0

#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
//sciagnac notepad++

#define SW (1<<PC5) //liczba 0b00000001 zostaje przesunieta o 5 miejsc w lewo 0b00100000, czyli 1<<PC5 to samo co 0b00100000
#define KEY_DOWN !(PINC & SW) //makro sprawdzajace stan niski, poczytac sobie na ten temat
#define TOGLE_LED (PORTB ^= LED) //zmiana ledu na przeciwny

//if #define SW (1<<PC5) //liczba 0b00000001 zostaje przesunieta o 5 miejsc w lewo 0b00100000, czyli 1<<PC5 to samo co 0b00100000
//#define KEY_DOWN !(PINC & SW) //PINx jest to rejestr sPINC = 0bxx0xxxxxluzacy do odczytywania stanu pinu WEJSCIOWEGO w tym przypadku pinu na porcie C. Jezeli
//przycisk wcisniety stan pinu5 w PINC = 0, to znaczy PINC = 0bxx0xxxxx (gdzie x to 1 lub 0 w zaleznosci jak pozostale piny zostaly ustawione).
// Piszac w ten sposob sprawdzamy tylko jeden interesujacy nas pin, czyli zakladamy tzw. maske na ten jeden bit. Jezeli przycisk wcisniety PINC5=0.
// Czyli #define KEY_DOWN !(0 & 1). Mamy tutaj iloczyn logiczny, z tabeli prawdy wynika, �e 0&1 daje 0. Pozostaje !(0) = 1, poniewa� ! to negacja.
// Uzywamy negacji po to bysmy mogli w latwy sposob korzystac z if, poniewaz wejscie do if nastepuje gdy w srodku if jest prawda, czyli logiczne 1.


int main(void)
{
	
    /* Replace with your application code */
	DDRB = 0b00000001; //0b oznacza zapis binarny (dwojkowy), 0x oznacza zapis 16stkowy
	PORTB = 0b00000001;
	DDRC = 0; //ustawiamy przycisk jako wejscie
	PORTC |= SW;
	_delay_ms(10); //delay dla powyzszych ustawien
	/* Replace with your application code */
	while (1) // nieskonczona petla, zawsze prawda
	{
		
		if (KEY_DOWN) //jezeli przycisk wcisniety
		{
			_delay_ms(100); //poczekaj mozliwe ze to tylko drganie stykow
			if (KEY_DOWN) //jezeli po tym czasie przycisk nadal wcisniety
			{
				//tutaj zmien led na przeciwny
				//dodaj delay na 200ms, na odcisniecie przycisku
			}		
		}	
	}
}

//Zadanie
//Stworzyc program w ktorym naciskajac przycisk zmieniasz stan ledu


